<?php

/**
 * Plugin Name: MoIP Spliter
 * Plugin URI: http://www.claytonduarte.com/split-moip
 * Description: Inclui o split de pagamentos no MoIP 
 * Version: 0.1
 * Author: Clayton Duarte
 * Author URI: http://www.claytonduarte.com
**/

function split_moip($xml, $order) {
	$addcomission = $xml->instruction->addChild('Comissoes');
	$addcomissioning = $addcomission->addChild('Comissionamento');
	$addcomissioning->addChild('Razao', 'Descrição');
	//$addcomissioning->addChild('ValorFixo', 10);
	$addcomissioning->addChild('ValorPercentual', 10);
    $addcomissioned = $addcomissioning->addChild('Comissionado');
    $addcomissioned->addChild('LoginMoIP', 'login_aqui');
	return $xml;
}

add_filter('woocommerce_moip_xml', 'split_moip', 10, 2);

//include "controle.php";

?>